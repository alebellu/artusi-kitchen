/* LICENSE START */
/*
 * ssoup
 * https://github.com/alebellu/ssoup
 *
 * Copyright 2012 Alessandro Bellucci
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://alebellu.github.com/licenses/GPL-LICENSE.txt
 * http://alebellu.github.com/licenses/MIT-LICENSE.txt
 */
/* LICENSE END */

/* HEADER START */
var env, requirejs, define;
if (typeof module !== 'undefined' && module.exports) {
    env = "node";
    requirejs = require('requirejs');
    define = requirejs.define;

    // require all dependant libraries, so they will be available for the client to request.
    var pjson = require('./package.json');
    for (var library in pjson.dependencies) {
        require(library);
    }
} else {
    env = "browser";
    //requirejs = require;
}

({ define: env === "browser"
        ? define
        : function(A,F) { // nodejs.
            var path = require('path');
            var moduleName = path.basename(__filename, '.js');
            console.log("Loading module " + moduleName);
            module.exports = F.apply(null, A.map(require));
            global.registerUrlContext("/" + moduleName + ".js", {
                "static": __dirname,
                defaultPage: moduleName + ".js"
            });
            global.registerUrlContext("/" + moduleName, {
                "static": __dirname
            });
        }
}).
/* HEADER END */

define(['jquery', 'artusi-kitchen-tools'], function($, tools) {

    var drdf = tools.drdf;
    var drff = tools.drff;

    var kitchenType = function(options) {
        var kitchen = this;
        kitchen.initOptions = options;

        kitchen.auths = {};
        kitchen.drawers = {};
        kitchen.hobs = {};
        kitchen.elevators = {};

        kitchen.tools = tools;

        // create a new context.
        kitchen.context = {
            kitchen: kitchen
        };
    };

    kitchenType.prototype.getIRI = function() {
        var kitchen = this;
        return kitchen.initOptions.iri;
    };

    kitchenType.prototype.open = function(options) {
        var dr = $.Deferred();
        var kitchen = this;
        var context = kitchen.context;

        //requirejs( [ 'artusi-worktop', 'artusi-kitchen-assistant', 'artusi-shelf', 'text!' + kitchen.getIRI() ],
        kitchen.loadKitchenConfig().done(function() {
            requirejs( [ 'artusi-kitchen-assistant', 'artusi-drawer', 'artusi-hob', 'artusi-pan', 'artusi-worktop', 'artusi-elevator', 'artusi-shelf' ],
            function( assistantType, drawerType, hobType, panType, worktopType, elevatorType, shelfType ) {
                $.Deferred(function(idr) {
                    idr.pipe(function() {
                        // adds the worktop to the kitchen
                        kitchen.worktop = new worktopType(context);
                        return kitchen.worktop.init();
                    }).pipe(function() {
                        // adds a shelf to the kitchen
                        kitchen.shelf = new shelfType(context);
                        return kitchen.shelf.init();
                    }).pipe(function() {
                        return kitchen.addAuths();
                    }).pipe(function() {
                        return kitchen.addDrawersSerially();
                    }).pipe(function() {
                        kitchen.assistant = new assistantType(context, {});
                    }).pipe(function() {
                        return kitchen.addElevators(context);
                    }).pipe(function() {
                        return kitchen.addHobs();
                    }).done(drdf(dr)).fail(drff(dr));
                }).resolve();
            } );
        });

        return dr.promise();
    };

    kitchenType.prototype.loadKitchenConfig = function() {
        var dr = $.Deferred();
        var kitchen = this;

        if (env === "node" && kitchen.initOptions.configFile) {
            var fs = require('fs');
            fs.readFile(kitchen.initOptions.configFile, function read(err, data) {
                if (err) {
                    dr.reject(err);
                    return;
                }

                var conf;
                eval('conf = ' + data); // using eval in place of JSON parse in order to allow comments inside kitchen definition
                kitchen.conf = conf;

                dr.resolve();
            });
        } else {
            $.ajax( {
                url: kitchen.getIRI(),
                // dataType: "json",
                cache: false
            } ).done(function(conf) {
                eval("kitchen.conf = " + conf); // using eval to allow comments in configuration
                dr.resolve();
            } ).fail(drff(dr));
        }

        return dr.promise();
    };

    kitchenType.prototype.addAuths = function() {
        var dr = $.Deferred();
        var kitchen = this;
        var context = kitchen.context;

        if (!kitchen.conf["sk:auths"] || kitchen.conf["sk:auths"].length === 0) {
            dr.resolve();
            return dr.promise();
        }

        requirejs( [ 'artusi-auth' ], function( authType ) {
            var loadAuth = function(index) {
                authType.loadAuth(context, {
                    authConf: kitchen.conf["sk:auths"][index]
                }).done(function(auth) {
                    kitchen.auths[auth.getIRI()] = auth;
                    kitchen.worktop.addNode({
                        node: auth
                    });
                    if (index < kitchen.conf["sk:auths"].length - 1) {
                        loadAuth(index + 1);
                    }
                    else {
                        dr.resolve();
                    }
                });
            };

            loadAuth(0);
        });

        return dr.promise();
    };

    kitchenType.prototype.addDrawersInParallel = function() {
        var drs = [];
        var kitchen = this;
        var context = kitchen.context;

        if (!kitchen.conf["sk:drawers"] || kitchen.conf["sk:drawers"].length === 0) {
            var dr = $.Deferred();
            dr.resolve();
            return dr.promise();
        }

        $.each(kitchen.conf["sk:drawers"], function(index, drawerConf) {
            var dr = $.Deferred();
            require( [ 'artusi-drawer' ], function( drawerType ) {
                drawerType.loadDrawer(context, {
                    drawerConf: drawerConf
                }).done(function(drawer) {
                    kitchen.drawers[drawer.getIRI()] = drawer;
                    kitchen.worktop.addNode({
                        node: drawer
                    });
                    dr.resolve();
                }).fail( drff( dr ) );
            });
            drs.push(dr);
        });

        return $.when.apply($, drs).promise();
    };

    kitchenType.prototype.addDrawersSerially = function() {
        var dr = $.Deferred();
        var kitchen = this;
        var context = kitchen.context;

        if (!kitchen.conf["sk:drawers"] || kitchen.conf["sk:drawers"].length === 0) {
            dr.resolve();
            return dr.promise();
        }

        requirejs( [ 'artusi-drawer' ], function( drawerType ) {
            var loadDrawer = function(index) {
                drawerType.loadDrawer(context, {
                    drawerConf: kitchen.conf["sk:drawers"][index]
                }).done(function(drawer) {
                    kitchen.drawers[drawer.getIRI()] = drawer;
                    kitchen.worktop.addNode({
                        node: drawer
                    });
                    if (index < kitchen.conf["sk:drawers"].length - 1) {
                        loadDrawer(index + 1);
                    }
                    else {
                        dr.resolve();
                    }
                });
            };

            loadDrawer(0);
        });

        return dr.promise();
    };

    kitchenType.prototype.addHobs = function() {
        var drs = [];
        var kitchen = this;
        var context = kitchen.context;

        if ( kitchen.conf["sk:hobs"] ) {
            $.each(kitchen.conf["sk:hobs"], function(index, hobConf) {
                var dr = $.Deferred();
                requirejs( [ 'artusi-hob' ], function( hobType ) {
                    hobType.loadHob(context, {
                        hobConf: hobConf
                    }).done(function(hob) {
                        kitchen.hobs[hob.getIRI()] = hob;
                        kitchen.worktop.addNode({
                            node: hob
                        });
                        dr.resolve();
                    }).fail( drff( dr ) );
                });
                drs.push(dr);
            });
        }

        return $.when.apply($, drs).promise();
    };

    kitchenType.prototype.addElevators = function() {
        var drs = [];
        var kitchen = this;
        var context = kitchen.context;

        if ( kitchen.conf["sk:elevators"] ) {
            $.each(kitchen.conf["sk:elevators"], function(index, elevatorConf) {
                var dr = $.Deferred();
                requirejs( [ 'artusi-elevator' ], function( elevatorType ) {
                    elevatorType.loadElevator(context, {
                        elevatorConf: elevatorConf
                    }).done(function(elevator) {
                        kitchen.elevators[elevator.getIRI()] = elevator;
                        kitchen.worktop.addNode({
                            node: elevator
                        });
                        dr.resolve();
                    }).fail( drff( dr ) );
                });
                drs.push(dr);
            });
        }

        return $.when.apply($, drs).promise();
    };

    return kitchenType;
});
